import React from "react";
import Header from "../components/Header";
import Highlight from "../components/Highlight";
import Footer from "../components/Footer";
import Newsletter from "../components/Newsletter";
import Notification from "../components/Notification";
import { DataDummy } from "../data/State";
import { Row, Col } from "reactstrap";
class App extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      notification: true,
      newsletter: true
    };
  }
  componentDidMount(){
    const newsletter = JSON.parse(window.localStorage.getItem("newsletter"));
    const notification = JSON.parse(window.localStorage.getItem("notification"));
    if(newsletter != null){
      this.setState({ newsletter: newsletter });
    }
    if(notification != null){
      this.setState({ notification: notification });
    }
  }
  onDismissNewsletter() {
    this.setState({ newsletter: false });
    localStorage.setItem("newsletter", false);
  }
  onDismissNotification(){
    this.setState({ notification: false });
    localStorage.setItem("notification", false);
  }
  render() {
    return (
      <React.Fragment>
        {this.state.notification ? 
          <Notification onDismiss={() => this.onDismissNotification()}/>
        : ""}
        {this.state.notification ? 
          <Header marginTop={60}/>
        : <Header marginTop={0}/>}
        <div className="container-fluid">
          <Row>
            <Col md={2} lg={3} />
            <Col xs={12} md={8} lg={6}>
              <div className="highlight-panel-header margin-top-40 margin-bottom-40">
                <h2 className="center">How Can I Help You ?</h2>
                <p className="center">
                  Our work then targeted, best practices outcomes social
                  innovation synergy Venture philanthropy, revolutionary
                  inclusive policymaker relief. User-centered program areas
                  scale.
                </p>
              </div>
            </Col>
            <Col md={2} lg={3} />
          </Row>
          <Row>
            {DataDummy.map((data, index) => (
              <Highlight 
                key={index} 
                title={data.title} 
                desc={data.desc} 
                iconName={data.iconName}
              />
            ))}
          </Row>
        </div>
        {this.state.newsletter ? 
          <Newsletter 
            onNewsletter={this.state.newsletter}
            onDismiss={() => this.onDismissNewsletter()}
          />
          : ""}
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
