import React from "react"
import { Container, Row, Col, Button } from "reactstrap"
const Notification = ({onDismiss}) => {
  return(
    <div className="bg-white">
      <div className="notification">
        <Container>
          <Row>
            <Col md={2} />
            <Col md={7}>
              <p className="font-size-14">By accessing and using this website, you acknowledge that you have read and  
              understand our <span>Cookie Policy</span>, <span>Privacy Policy</span>, and our <span>Terms of Service</span>.</p>
            </Col>
            <Col md={2}>
              <Button color="primary" className="font-size-12 margin-top-10" onClick={onDismiss}>Got it</Button>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  )
}

export default Notification;