import React from "react";

const Footer = () => {
  return (
    <footer className="padding-10 margin-top-20">
      <p className="font-size-12 font-white margin-top-10 center">
        ​© 2019 Surendi Simamora. All rights reserved.​
      </p>
    </footer>
  );
};

export default Footer;
