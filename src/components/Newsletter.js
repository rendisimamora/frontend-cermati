import React from "react"
import { Col, Alert, Button } from "reactstrap";
const Newsletter = ({onNewsletter, onDismiss}) => {
  return (
    <Alert
      className="newsletter newsletter-dismissible"
      isOpen={onNewsletter}
      toggle={onDismiss}
    >
      <h5 className="font-white">Get latest updates in web technologies</h5>
      <p className="font-white font-size-12">
        I write articles related to web technologies, such as design trends,
        development tools, UI/UX case studies and reviews, and more. Sign up
        to my newsletter to get them all.
      </p>
      <div className="row">
        <Col xs={12} md={12} lg={8} xl={9} id="newsletter-left-box">
          <input
            type="email"
            name="email"
            placeholder="Email address"
            className="font-size-12"
          />
        </Col>
        <Col xs={12} md={12} lg={4} xl={3}>
          <Button
            type="submit"
            name="submit"
            className="btn-count font-size-12 font-bold"
          >
            Count me in!
          </Button> 
        </Col>
      </div>
    </Alert> 
  )
}

export default Newsletter;