import React from "react";
import { Col } from "reactstrap";
import * as FontAwesome from 'react-icons/lib/fa'
const Highlight = ({ title, desc, iconName }) => {
  const faIcon = React.createElement(FontAwesome[iconName]);
  return (
    <Col xs={12} md={6} lg={6} xl={4}>
      <div className="highlight-panel-content">
        <div className="highlight-panel-header">
          <h3>{title}</h3>
          <div className="highlight-panel-header-right">
            <p>{faIcon}</p>
          </div>
        </div>    
        <div className="highlight-panel-section">
          <p>{desc}</p>
        </div>
      </div>
    </Col>
  );
};
export default Highlight;
