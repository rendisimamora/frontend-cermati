import React from "react";

const Header = (marginTop) => {
  return (
    <header style={{marginTop: marginTop.marginTop}}>
      <img
        className="img-logo margin-30"
        src="https://res.cloudinary.com/rendisimamora/image/upload/v1558340713/Cermati/y-logo-white.png"
        alt="Logo"
      />
      <div className="hero__shot__section center">
        <h2 className="margin-bottom-0 font-weight-200 font-white">
          Hello! I'm Surendi Simamora
        </h2>
        <h4 className="font-weight-400 font-white">
          Consult, Design, and Develop Websites
        </h4>
        <p className="font-weight-100 margin-bottom-0 font-white">
          Have something great in mind? Feel free to contact me.
        </p>
        <p className="font-weight-100 font-white">
          I'll help you to make it happen.
        </p>
        <button className="btn-cermati-header bg-transparent padding-10 font-white font-size-14">
          LET'S MAKE CONTACT
        </button>
      </div>
    </header>
  );
};

export default Header;
